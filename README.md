﻿# LD52

This repository contains a game for Ludum Dare 52.
It is using the Godot game engine with my own [EctGodotUtils library/project template](https://github.com/ectucker1/EctGodotUtils).

The repository also contains copies of a couple other addons and other resources:
- [GodotExtraMath](https://github.com/aaronfranke/GodotExtraMath)
- [godot-plugin-refresher](https://github.com/godot-extended-libraries/godot-plugin-refresher)
