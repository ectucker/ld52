using Godot;
using System;

public class DrillAnim : Node2D
{
    private float _time = 0.0f;
    private Vector2 _startPos = Vector2.Zero;

    public override void _Ready()
    {
        base._Ready();

        _startPos = Position;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        Position = _startPos + (Mathf.Sin(_time * 10.0f) * 20.0f * Vector2.Up);
    }
}
