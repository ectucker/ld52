using Godot;
using System;

public class QuitButton : Button
{
    public override void _Ready()
    {
        base._Ready();

        if (OS.GetName() == "HTML5")
        {
            Visible = false;
        }
    }

    public override void _Pressed()
    {
        base._Pressed();
        
        GetTree().Quit();
    }
}
