using Godot;
using System;
using System.Threading.Tasks;

public class MainMenu : Node2D
{
    private AnimationPlayer _animation;

    private bool _starting = false;

    public override void _Ready()
    {
        base._Ready();

        _animation = GetNode<AnimationPlayer>("AnimationPlayer");
    }

    public void Start()
    {
        if (!_starting)
        {
            AsyncRoutine.Start(this, _startAnim);
            _starting = true;
        }
    }

    private async Task _startAnim(AsyncRoutine routine)
    {
        _animation.Play("DrillDown");
        await routine.Signal(_animation, SignalNames.ANIMATION_FINISHED);

        await routine.Delay(0.5f);
        
        GetTree().ChangeScene("res://level/level_main.tscn");
    }
}
