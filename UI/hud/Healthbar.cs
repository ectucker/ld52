using Godot;
using System;

public class Healthbar : Sprite
{
    private Hud _hud;

    public float CurPercent = 1.0f;
    private float time = 0.0f;

    private ShaderMaterial _material;

    private CPUParticles2D _healParticles;
    private CPUParticles2D _hurtParticles;

    public override void _Ready()
    {
        _hud = this.FindParent<Hud>();
        _hud.Connect(nameof(Hud.HealthChanged), this, nameof(HealthChanged));
        
        _material = Material as ShaderMaterial;
        
        _healParticles = GetParent().FindNode("HealParticles") as CPUParticles2D;
        _hurtParticles = GetParent().FindNode("HurtParticles") as CPUParticles2D;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        time += delta;
        _material.SetShaderParam("percentage", CurPercent);
        _material.SetShaderParam("time", time);
    }

    private void HealthChanged()
    {
        if (_hud.HealthPercent > CurPercent)
        {
            _healParticles.Emitting = true;
            var tween = GetTree().CreateTween();
            tween.TweenInterval(0.4f);
            tween.TweenProperty(this, nameof(CurPercent), _hud.HealthPercent, 0.4f).SetTrans(Tween.TransitionType.Linear);
        }
        else if (_hud.HealthPercent < CurPercent)
        {
            _hurtParticles.Emitting = true;
            var tween = GetTree().CreateTween();
            tween.TweenInterval(0.2f);
            tween.TweenProperty(this, nameof(CurPercent), _hud.HealthPercent, 0.4f).SetTrans(Tween.TransitionType.Expo);
        }
    }
}
