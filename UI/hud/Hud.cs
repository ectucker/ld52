using Godot;
using System;

public class Hud : Control
{
    public static Hud Instance;

    [Signal]
    public delegate void HealthChanged();
    
    [Signal]
    public delegate void DepthChanged();
    
    [Signal]
    public delegate void ScoreChanged();

    private float _healthPercent = 1.0f;
    public float HealthPercent
    {
        get => _healthPercent;
        set
        {
            _healthPercent = value;
            EmitSignal(nameof(HealthChanged));
        }
    }
    
    private float _depth = 0.0f;
    public float Depth
    {
        get => _depth;
        set
        {
            _depth = value;
            GlobalScores.Depth = value;
            EmitSignal(nameof(DepthChanged));
        }
    }
    
    private int _score = 0;
    public int Score
    {
        get => _score;
        set
        {
            _score = value;
            GlobalScores.Score = value;
            EmitSignal(nameof(ScoreChanged));
        }
    }

    public override void _Ready()
    {
        base._Ready();

        Instance = this;
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        Instance = null;
    }
}
