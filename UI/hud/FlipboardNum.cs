using Godot;
using System;

public class FlipboardNum : Control
{
    public float _actualNum = 0;
    public float _num = 0;

    private Sprite _digits;

    public override void _Ready()
    {
        base._Ready();

        _digits = this.FindNode("Digits") as Sprite;
        _digits.Material = _digits.Material.Duplicate() as Material;
    }

    public void GotoNum(int num)
    {
        float target = num;
        if (target < _actualNum)
            target += 10.0f;
        _actualNum = target;
        
        GetTree().CreateTween().TweenProperty(this, nameof(_num), target, 0.1f);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_digits.Material is ShaderMaterial shader)
        {
            shader.SetShaderParam("pos", _num);
        }
    }
}
