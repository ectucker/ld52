using Godot;
using System;

public class ScoreValue : FlipboardValue
{
    private Hud _hud;
    
    public override void _Ready()
    {
        base._Ready();
        
        _hud = this.FindParent<Hud>();
        
        _hud.Connect(nameof(Hud.ScoreChanged), this, nameof(ScoreChanged));
    }

    private void ScoreChanged()
    {
        SetValue(_hud.Score);
    }
}