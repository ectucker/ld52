using Godot;
using System;

public class MultValue : FlipboardValue
{
    private Hud _hud;
    
    public override void _Ready()
    {
        base._Ready();
        
        _hud = this.FindParent<Hud>();
        
        _hud.Connect(nameof(Hud.DepthChanged), this, nameof(DepthChanged));
    }

    private void DepthChanged()
    {
        SetValue(Mathf.RoundToInt(GlobalScores.Multiplier * 100.0f));
    }
}
