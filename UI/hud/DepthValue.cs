using Godot;
using System;

public class DepthValue : FlipboardValue
{
    private Hud _hud;
    
    public override void _Ready()
    {
        base._Ready();
        
        _hud = this.FindParent<Hud>();
        
        _hud.Connect(nameof(Hud.DepthChanged), this, nameof(DepthChanged));
    }

    private void DepthChanged()
    {
        SetValue(GlobalScores.DepthDisplay(_hud.Depth));
    }
}
