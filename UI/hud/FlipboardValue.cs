﻿using System;
using System.Collections.Generic;
using System.Text;
using Godot;

public class FlipboardValue : Node
{
    private List<FlipboardNum> _digits;

    private int _lastVal = 0;

    private string _format;

    public override void _Ready()
    {
        base._Ready();

        _digits = new List<FlipboardNum>(this.FindChildren<FlipboardNum>());

        StringBuilder formatBuilder = new StringBuilder();
        formatBuilder.Append("{0:");
        foreach (var digit in _digits)
            formatBuilder.Append("0");
        formatBuilder.Append("}");
        _format = formatBuilder.ToString();
    }

    public void SetValue(int value)
    {
        if (value != _lastVal)
        {
            _lastVal = value;
            string padded = String.Format(_format, value);
            for (int i = 0; i < _digits.Count; i++)
            {
                _digits[i].GotoNum(Int32.Parse(padded[i].ToString()));
            }
        }
    }
}
