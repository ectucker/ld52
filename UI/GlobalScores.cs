using Godot;
using System;

public class GlobalScores
{
    private static float _depth = 0.0f;
    public static float Depth
    {
        get => _depth;
        set
        {
            _depth = value;
            MaxDepth = Mathf.Max(MaxDepth, _depth);
        }
    }

    private static int _score = 0;
    public static int Score
    {
        get => _score;
        set
        {
            _score = value;
            HighScore = Mathf.Max(HighScore, _score);
        }
    }

    public static float Multiplier => 1.0f + Depth / 10000.0f;

    public static bool Finished = false;

    public static int HighScore = 0;
    public static float MaxDepth = 0;
    
    public static int DepthDisplay(float depth)
    {
        return Mathf.RoundToInt(depth / 5.0f);
    }

    public static float ScoreUpTime()
    {
        return Mathf.Clamp((Score / 25000.0f * 2.0f) + 1.0f, 2.0f, 10.0f) ;
     
    }

    public static float DepthUpTime()
    {
       
        return Mathf.Clamp((Depth / 40000.0f * 3.0f) + 1.0f, 2.0f, 10.0f);
       
       
    }

    public static float MaxUpTime()
    {
        return Mathf.Max(ScoreUpTime(), DepthUpTime());
    }

    public static void Reset()
    {
        Depth = 0;
        Score = 0;
        Finished = false;
    }
}
