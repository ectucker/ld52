using Godot;
using System;

public class VolumeOptions : Control
{
    private Range _mainVolumeRange;
    private Range _effectsVolumeRange;
    private Range _musicVolumeRange;

    public override void _Ready()
    {
        base._Ready();
        
        _mainVolumeRange = FindNode("MainSlider") as Range;
        _mainVolumeRange.Value = AudioSettings.MainVolume;
        _mainVolumeRange.Connect(SignalNames.RANGE_VALUE_CHANGED, this, nameof(UpdateVolumes));
        _effectsVolumeRange = FindNode("EffectsSlider") as Range;
        _effectsVolumeRange.Value = AudioSettings.EffectsVolume;
        _effectsVolumeRange.Connect(SignalNames.RANGE_VALUE_CHANGED, this, nameof(UpdateVolumes));
        _musicVolumeRange = FindNode("MusicSlider") as Range;
        _musicVolumeRange.Value = AudioSettings.MusicVolume;
        _musicVolumeRange.Connect(SignalNames.RANGE_VALUE_CHANGED, this, nameof(UpdateVolumes));
    }

    private void UpdateVolumes(float _)
    {
        AudioSettings.MainVolume = (float)_mainVolumeRange.Value;
        AudioSettings.EffectsVolume = (float)_effectsVolumeRange.Value;
        AudioSettings.MusicVolume = (float)_musicVolumeRange.Value;
    }
}
