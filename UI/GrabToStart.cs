using Godot;
using System;

public class GrabToStart : Button
{
    private MainMenu _mainMenu;

    public override void _Ready()
    {
        base._Ready();

        _mainMenu = this.FindParent<MainMenu>();

        Disabled = true;
    }

    public override void _Pressed()
    {
        base._Pressed();
        
        _mainMenu.Start();
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (@event.IsActionPressed("grab"))
        {
            this.Pressed = true;
            _Pressed();
            EmitSignal("pressed");
        }
    }
}