using Godot;
using System;

public class StartButton : Button
{
    private MainMenu _mainMenu;

    public override void _Ready()
    {
        base._Ready();

        _mainMenu = this.FindParent<MainMenu>();
    }

    public override void _Pressed()
    {
        base._Pressed();
        
        _mainMenu.Start();
    }
}
