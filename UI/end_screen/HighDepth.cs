using Godot;
using System;
using System.Threading.Tasks;

public class HighDepth : Label
{
    public override void _Ready()
    {
        base._Ready();

        AsyncRoutine.Start(this, Show);
    }

    private async Task Show(AsyncRoutine routine)
    {
        Text = string.Format("(Max: {0})", GlobalScores.DepthDisplay(GlobalScores.MaxDepth));
        await routine.Delay(GlobalScores.MaxUpTime());
        
        GetTree().CreateTween().TweenProperty(this, "percent_visible", 1.0f, 0.5f);
    }
}
