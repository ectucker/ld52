using Godot;
using System;

public class PlayAgain : Button
{
    public override void _Ready()
    {
        base._Ready();
        
        PauseMenu.Enabled = false;
    }

    public override void _Pressed()
    {
        base._Pressed();
        
        Transition.TransitionTo("res://level/level_main.tscn");
    }
}
