using Godot;
using System;

public class PlayMaxUpTime : AudioStreamPlayer
{
    private float _time = 0.0f;

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        if (_time < GlobalScores.MaxUpTime())
        {
            if (!Playing)
                Play();
        }
        else
        {
            Stop();
        }
    }
}