using Godot;
using System;

public class DepthEndValue : FlipboardValue
{
    private Hud _hud;

    private float _value = 0.0f;
    
    public override void _Ready()
    {
        base._Ready();

        GetTree().CreateTween().TweenProperty(this, nameof(_value), (float)GlobalScores.DepthDisplay(GlobalScores.Depth), GlobalScores.DepthUpTime());
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        SetValue(Mathf.RoundToInt(_value));
    }
}