using Godot;
using System;

public class LoopUntil : AudioStreamPlayer
{
    private float _time = 0.0f;

    [Export()] private float _duration = 3.0f;

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        if (_time < _duration)
        {
            if (!Playing)
                Play();
        }
    }
}
