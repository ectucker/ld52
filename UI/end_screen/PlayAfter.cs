using Godot;
using System;

public class PlayAfter : AudioStreamPlayer
{
    [Export()] private float _delay = 0.0f;

    private float _time = 0.0f;
    
    public override void _Ready()
    {
        
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        if (_time >= _delay)
        {
            Play();
            SetProcess(false);
        }
    }
}
