using Godot;
using System;
using System.Threading.Tasks;

public class DepthRecord : Control
{

    private AnimationPlayer _anim;

    [Export()]
    private NodePath _chimePath;
    
    public override void _Ready()
    {
        _anim = this.FindChild<AnimationPlayer>();
        AsyncRoutine.Start(this, ShowRecord);
    }

    private async Task ShowRecord(AsyncRoutine routine)
    {
        await routine.Delay(GlobalScores.MaxUpTime() + 0.5f);

        if (GlobalScores.DepthDisplay(GlobalScores.Depth) >= GlobalScores.DepthDisplay(GlobalScores.MaxDepth))
        {
            _anim.Play("Show");
            GetNode<AudioStreamPlayer>(_chimePath).Play();
        }
    }
}
