using Godot;
using System;

public class MainMenuButton : Button
{
    public override void _Pressed()
    {
        base._Pressed();
        
        Transition.TransitionTo("res://UI/main_menu/main_menu.tscn");
    }
}
