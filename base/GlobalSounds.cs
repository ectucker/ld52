using Godot;
using System;

public class GlobalSounds : Node
{
    public static GlobalSounds Instance;

    public AudioStreamProxy GemPickup;

    public override void _Ready()
    {
        base._Ready();
        
        Instance = this;
        GemPickup = new AudioStreamProxy(GetNode("Pickup"));
    }
}
