using Godot;
using System;
using System.Collections.Generic;
using PackedScene = Godot.PackedScene;

public class HazardSpawner : Node
{
    private class SpawnQueued
    {
        public Action Spawn;
        public float Depth;

        public SpawnQueued(float depth, Action spawn)
        {
            Depth = depth;
            Spawn = spawn;
        }
    }
    
    [Export]
    private NodePath _spawnCenterPath;
    private Node2D _spawnCenter;

    public float Depth => _spawnCenter.GlobalPosition.y;

    private List<SpawnQueued> _spawnQueue = new List<SpawnQueued>();

    // Scenes should all be loaded in ready to avoid reloading
    private PackedScene _rockScene;

    private PackedScene _flowerScene;

    private PackedScene _wormScene;

    private PackedScene _batScene;

    private PackedScene _mineScene;

    private PackedScene _gemScene;

    private PackedScene _healthScene;
    public override void _Ready()
    {
        _spawnCenter = GetNode<Node2D>(_spawnCenterPath);

        // doesn't seem to 
        _rockScene = GD.Load<PackedScene>("res://enemies/evilRock.tscn");
        _flowerScene = GD.Load<PackedScene>("res://enemies/flower.tscn");
        _wormScene = GD.Load<PackedScene>("res://enemies/worm.tscn");
        _batScene = GD.Load<PackedScene>("res://enemies/bats.tscn");
        _mineScene = GD.Load<PackedScene>("res://enemies/mine.tscn");
        _gemScene = GD.Load<PackedScene>("res://world/pickups/gem25.tscn");
        _healthScene = GD.Load<PackedScene>("res://world/pickups/health.tscn");
        
        _spawnQueue.Add(new SpawnQueued(1500.0f, SpawnRock));
        _spawnQueue.Add(new SpawnQueued(2500.0f, SpawnFlower));
        _spawnQueue.Add(new SpawnQueued(3500.0f, SpawnWorm));
        _spawnQueue.Add(new SpawnQueued(4500.0f, SpawnBat));
        _spawnQueue.Add(new SpawnQueued(2280.0f, SpawnMine));
        _spawnQueue.Add(new SpawnQueued(750.0f, SpawnGem));
        _spawnQueue.Add(new SpawnQueued(1800.0f, SpawnHeatlh));
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
        DebugOverlay.AddMessage(this, "Cur Depth", Depth.ToString(), Colors.Gold);
        List<SpawnQueued> copy = new List<SpawnQueued>(_spawnQueue);
        foreach (var queued in copy)
        {
            if (queued.Depth < Depth)
            {
                queued.Spawn();
                _spawnQueue.Remove(queued);
            }
        }
    }

    private void SpawnRock()
    {
        // Instance rock
        Node2D rock = _rockScene.Instance<Node2D>();
        // Add rock to parent
        // Use _spawnCenter if it should be tied to depth for some reason
        
        _spawnCenter.AddChild(rock);
        // Set position
        rock.GlobalPosition = new Vector2(rock.GlobalPosition.x, Depth + 200.0f);
        
        // Queue up another one for 200-400 pixels from now
        _spawnQueue.Add(new SpawnQueued(Depth + (float)GD.RandRange(370.0f, 800.0f), SpawnRock));
    }

    private void SpawnFlower()
    {
        Node2D flower = _flowerScene.Instance<Node2D>();
        
     
        _spawnCenter.AddChild(flower);
        flower.GlobalPosition = new Vector2(flower.GlobalPosition.x, Depth + 850.00f);
        _spawnQueue.Add(new SpawnQueued(Depth + (float)GD.RandRange(1000.0f, 2500.0f) * 2f, SpawnFlower));
    }

    private void SpawnWorm()
    {
        Node2D worm = _wormScene.Instance<Node2D>();
       
        _spawnCenter.AddChild(worm);
        worm.GlobalPosition = new Vector2(worm.GlobalPosition.x, Depth- (float) GD.RandRange(100, 900));
        _spawnQueue.Add((new SpawnQueued(Depth + (float)GD.RandRange(2000f, 4000f) * 1.7f, SpawnWorm)));
    }

    private void SpawnBat()
    {
        Node2D bat = _batScene.Instance<Node2D>();
  
        _spawnCenter.AddChild(bat);
        bat.GlobalPosition = new Vector2(bat.GlobalPosition.x, Depth - 500);
        _spawnQueue.Add((new SpawnQueued(Depth + (float)GD.RandRange(1000f, 4000f) * 1.6f, SpawnBat)));
    }

    private void SpawnMine()
    {
        Node2D mine = _mineScene.Instance<Node2D>();
        
        _spawnCenter.AddChild(mine);
        mine.GlobalPosition = new Vector2(mine.GlobalPosition.x, Depth + 250.0f);
     
        _spawnQueue.Add((new SpawnQueued(Depth + (float)GD.RandRange(1000f, 2000f) * 1.2f, SpawnMine)));
    }
    
    private void SpawnGem()
    {
        
        Node2D gem = _gemScene.Instance<Node2D>();
        
        _spawnCenter.AddChild(gem);
    
        gem.GlobalPosition = new Vector2(gem.GlobalPosition.x, Depth + 200.0f);
        
    
        _spawnQueue.Add(new SpawnQueued(Depth + (float)GD.RandRange(250.0f, 550.0f), SpawnGem));
    }
    
    private void SpawnHeatlh()
    {
        
        Node2D health = _healthScene.Instance<Node2D>();
        
        _spawnCenter.AddChild(health);
    
        health.GlobalPosition = new Vector2(health.GlobalPosition.x, Depth + 200.0f);
        
    
        _spawnQueue.Add(new SpawnQueued(Depth + (float)GD.RandRange(400.0f, 1000.0f) * 1.2f , SpawnHeatlh));
    }
    
}

