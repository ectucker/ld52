using Godot;
using System;

public class CurrentDepth : Position2D
{
    private static CurrentDepth _instance;

    public static float Depth
    {
        get
        {
            if (_instance != null)
                return _instance.GlobalPosition.y;
            else
                return 1000;
        }
    }

    public override void _Ready()
    {
        base._Ready();

        _instance = this;
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _instance = null;
    }
}
