using Godot;
using System;

public class SlimDisplay : Node2D
{
    [Export()]
    private NodePath _slimPath;

    private Node2D _slim;

    private float _time = 0.0f;
    
    public override void _Ready()
    {
        _slim = GetNode<Node2D>(_slimPath);
        Visible = false;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        if (_time > 3.0f)
        {
            GlobalPosition = new Vector2(_slim.GlobalPosition.x, GlobalPosition.y);
            Visible = _slim.GlobalPosition.y < GlobalPosition.y;
        }
    }
}
