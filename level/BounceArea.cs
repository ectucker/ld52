using Godot;
using System;

public class BounceArea : Area2D
{
    [LiveValue(LVType.RANGE, 0.0f, 3000.0f, "World")]
    public static float BOUNCE_SPEED = 1000.0f;

    private AAudioStreamCollection _bounceSound;

    private CPUParticles2D _bounceParticles;
    
    public override void _Ready()
    {
        Connect(SignalNames.AREA2D_BODY_ENTERED, this, nameof(_BodyEntered));
        _bounceSound = this.FindChild<AAudioStreamCollection>();
        _bounceParticles = this.FindChild<CPUParticles2D>();
    }

    public virtual void _BodyEntered(Node node)
    {
        if (node is Player player)
        {
            player.Velocity = new Vector2(player.Velocity.x, 0.0f);
            player.Velocity += Vector2.Up * BOUNCE_SPEED;
            _bounceSound.Play();
            DebugOverlay.AddMessage(this, "Bounce", "Bounce", Colors.Purple, 1, 0.5f);
            _bounceParticles.GlobalPosition = new Vector2(player.GlobalPosition.x, _bounceParticles.GlobalPosition.y);
            _bounceParticles.Restart();
            _bounceParticles.Emitting = true;
        }
    }
}
