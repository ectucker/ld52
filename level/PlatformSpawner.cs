using Godot;
using System;
using System.Collections.Generic;
using PackedScene = Godot.PackedScene;

public class PlatformSpawner : Node
{
    private class PlatformOscillator
    {
        private OpenSimplexNoise _noise;

        private float _nextSample;
        
        public Vector2 LastSpawn = Vector2.Zero;

        public PlatformOscillator(OpenSimplexNoise noise, float startDepth)
        {
            _noise = noise;
            _nextSample = startDepth;
        }

        public bool ShouldSample(float depth)
        {
            return depth > _nextSample;
        }
        
        public float SampleX(float depth)
        {
            _nextSample = depth + (float)GD.RandRange(PLATFORM_SAMPLE_MIN, PLATFORM_SAMPLE_MAX);
            float x = _noise.GetNoise1d(depth) * LevelConsts.LEVEL_RADIUS * 2.5f;
            return x;
        }
    }

    private List<PlatformOscillator> _oscillators = new List<PlatformOscillator>();

    private PackedScene[] _platforms;

    [Export]
    private NodePath _spawnCenterPath;

    private Node2D _spawnCenter;

    private List<Node2D> _spawned = new List<Node2D>();
    private float _nextSpawnCheck = 200.0f;

    [LiveValue(LVType.RANGE, 0.0f, 100.0f, "PlatformGen")]
    public static float PLATFORM_SAMPLE_MIN = 400.0f;
    
    [LiveValue(LVType.RANGE, 0.0f, 1000.0f, "PlatformGen")]
    public static float PLATFORM_SAMPLE_MAX = 700.0f;
    
    [LiveValue(LVType.RANGE, 0.0f, 1000.0f, "PlatformGen")]
    public static float PLATFORM_SKIP_DIST = 350.0f;

    [LiveValue(LVType.RANGE, -50.0f, 0.0f, "PlatformGen")]
    public static float PLATFORM_MIN_ROT_DEG = -180.0f;

    [LiveValue(LVType.RANGE, 0.0f, 50.0f, "PlatformGen")]
    public static float PLATFORM_MAX_ROT_DEG = 180.0f;

    public override void _Ready()
    {
        _spawnCenter = GetNode<Node2D>(_spawnCenterPath);
        
        _platforms = new PackedScene[]
        {
            GD.Load<PackedScene>("res://world/platforms/platform1.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform2.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform3.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform4.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform5.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform6.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform7.tscn"),
            GD.Load<PackedScene>("res://world/platforms/platform8.tscn")
        };
        
        GD.Randomize();
        
        AddOscillator(4, 200.0f, 0.8f, 4.0f, 0.0f);
        AddOscillator(4, 100.0f, 0.5f, 2.0f, 50.0f);
        AddOscillator(4, 150.0f, 0.3f, 3.0f, 76.0f);
        AddOscillator(4, 100.0f, 0.6f, 2.0f, 100.0f);
        AddOscillator(4, 150.0f, 0.3f, 3.0f, 76.0f);
        AddOscillator(4, 100.0f, 0.6f, 2.0f, 100.0f);
    }

    private void AddOscillator(int octaves, float period, float persistence, float lacunarity, float startDepth)
    {
        var noise1 = new OpenSimplexNoise();
        noise1.Seed = (int)GD.Randi();
        noise1.Octaves = octaves;
        noise1.Lacunarity = lacunarity;
        noise1.Period = period;
        noise1.Persistence = persistence;
        _oscillators.Add(new PlatformOscillator(noise1, startDepth));
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        float depth = _spawnCenter.GlobalPosition.y;
        foreach (var oscillator in _oscillators)
        {
            if (oscillator.ShouldSample(depth))
            {
                float x = oscillator.SampleX(depth);
                bool skip = false;
                foreach (var other in _oscillators)
                {
                    if ((other.LastSpawn - new Vector2(x, depth)).Length() < PLATFORM_SKIP_DIST)
                    {
                        skip = true;
                    }
                }

                if (!skip)
                {
                    oscillator.LastSpawn = new Vector2(x, depth);
                    SpawnPlatform(depth, x);
                }
            }
        }

        if (_nextSpawnCheck < depth)
        {
            _nextSpawnCheck += 100.0f;
            List<Node2D> _toRemove = new List<Node2D>();
            foreach (var spawn in _spawned)
            {
                if (spawn.GlobalPosition.y < depth - 1300)
                {
                    _toRemove.Add(spawn);
                }
            }

            foreach (var remove in _toRemove)
            {
                _spawned.Remove(remove);
                remove.QueueFree();
            }
        }
    }

    public void SpawnPlatform(float depth, float x)
    {
        long pick = GD.Randi() % _platforms.Length;
        Node2D platform = _platforms[pick].Instance<Node2D>();
        GetParent().AddChild(platform);
        platform.GlobalPosition = new Vector2(x, depth + 500.0f);
        platform.GlobalRotation = (float)GD.RandRange(Mathf.Deg2Rad(PLATFORM_MIN_ROT_DEG), Mathf.Deg2Rad(PLATFORM_MAX_ROT_DEG));
        float scaleSize = (float)GD.RandRange(85, 100) * 0.01f;
        platform.Scale = new Vector2(scaleSize, scaleSize);
        platform.ZIndex = -1;
        _spawned.Add(platform);
    }
}
