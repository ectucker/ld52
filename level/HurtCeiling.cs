using Godot;
using System;

public class HurtCeiling : enemyBaseClass
{
    public override void DamagePlayer(Player player)
    {
        player.Damage(this, 4.0f);
    }
}
