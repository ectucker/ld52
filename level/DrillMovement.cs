using Godot;
using System;

public class DrillMovement : Node2D
{
    [LiveValue(LVType.RANGE, 0.0f, 2000.0f, "World")]
    public static float DRILL_SPEED = 1000.0f;

    private float speedIncrease = 1.0f;

    private float speedIncreseMultiplier = 0.004f;
    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
        
        if (!GlobalScores.Finished)
        {
            
            DebugOverlay.AddMessage(this, "DrillSpeed", ( speedIncrease * delta * Vector2.Down * DRILL_SPEED).ToString(), 1,1.0f);
            speedIncrease += speedIncrease * speedIncreseMultiplier * delta;
            GlobalPosition +=  speedIncrease * delta * Vector2.Down * DRILL_SPEED;

            Hud.Instance.Depth = GlobalPosition.y;
        }
    }
}
