using Godot;
using System;

/// <summary>
/// A script for a node that plays sounds when a button is hovered on or pressed.
/// Sounds may be any kind of audio stream, collection, etc.
/// </summary>
public class ButtonSounds : Node
{
	private AudioStreamProxy _hoverSound;
	private AudioStreamProxy _pressedSound;
	
	public override void _Ready()
	{
		base._Ready();

		_hoverSound = new AudioStreamProxy(GetNode("Hover"));
		_pressedSound = new AudioStreamProxy(GetNode("Press"));

		if (GetParent() is BaseButton button)
		{
			button.Connect(SignalNames.CONTROL_MOUSE_ENTERED, this, nameof(_Hover));
			button.Connect(SignalNames.BUTTON_PRESSED, this, nameof(_Pressed));
		}
		else if (GetParent() is Slider slider)
		{
			slider.Connect(SignalNames.CONTROL_MOUSE_ENTERED, this, nameof(_Hover));
			slider.Connect("drag_ended", this, nameof(_DragEnded));
		}
	}

	private void _Hover()
	{
		_hoverSound.Play();
	}

	private void _Pressed()
	{
		_pressedSound.Play();
	}
	
	private void _DragEnded(bool changed)
	{
		_pressedSound.Play();
	}
}
