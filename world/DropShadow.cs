using Godot;
using System;

public class DropShadow : Sprite
{
    private Sprite _shadow;
    
    public override void _Ready()
    {
        base._Ready();

        _shadow = new Sprite();
        AddChild(_shadow);
        _shadow.Texture = Texture;
        _shadow.Modulate = new Color(0, 0, 0, 0.3f);
        _shadow.GlobalTransform = GlobalTransform;
        _shadow.ZIndex = -1;
        PauseMode = PauseModeEnum.Process;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _shadow.GlobalTransform = GlobalTransform;
        _shadow.GlobalPosition = GlobalPosition + new Vector2(8, 8);
    }
}
