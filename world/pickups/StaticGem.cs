using Godot;
using System;
using System.Threading.Tasks;

public class StaticGem : Area2D
{
    [Export]
    private int _points = 1;
    
    private bool pickedUp = false;

    private PackedScene _pickupTextScene;
    
    public override void _Ready()
    {
        Connect(SignalNames.AREA2D_BODY_ENTERED, this, nameof(_BodyEntered));
        CallDeferred(nameof(RemoveOthers));
        _pickupTextScene = GD.Load<PackedScene>("res://world/pickups/pickup_text.tscn");
    }

    private void RemoveOthers()
    {
        int count = 0;
        int above = 0;
        foreach (var gem in GetParent().FindChildren<StaticGem>())
        {
            count += 1;
            if (gem.GlobalPosition.y < GlobalPosition.y)
            {
                above += 1;
            }
        }

        if (above > 0.3f * count)
        {
            QueueFree();
        }
    }

    private void _BodyEntered(Node body)
    {
        if (body is Player player && !pickedUp)
        {
            player.GivePoints(this, _points);
            AsyncRoutine.Start(this, Pickup);
            PickupText text = _pickupTextScene.Instance<PickupText>();
            text.Text = Mathf.CeilToInt(this._points * 10 * GlobalScores.Multiplier).ToString();
            GetParent().GetParent().AddChild(text);
            text.GlobalPosition = GlobalPosition;
            pickedUp = true;
        }
        else if (body != GetParent() && !pickedUp)
        {
            QueueFree();
        }
    }

    private async Task Pickup(AsyncRoutine routine)
    {
        SetDeferred("Monitoring", false);
        SetDeferred("Monitorable", false);
        Visible = false;
        GlobalSounds.Instance.GemPickup.Play();

        await routine.Delay(1.0f);
        
        QueueFree();
    }
}
