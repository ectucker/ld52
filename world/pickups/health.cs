using Godot;
using System;
using System.Threading.Tasks;


public class health : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    
    private int rotationSpeed;

    private const int mapEdge = 600;
    
    [LiveValue(LVType.RANGE, 0.0f, 30.0f, "Health Pickup")]
    public static float shootSpeed = 13;

    private int shootSpeedMod;
    
    private int shootAngle;

    private int shootLocation;

    private Vector2 vel = new Vector2();

    private int sideSpeed = 10;
    
    private float grav = 700;

    private bool _pickedUp = false;
    
    private PackedScene _pickupTextScene;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Connect(SignalNames.AREA2D_BODY_ENTERED, this, nameof(_BodyEntered));
        
        base._Ready();
        
        Random r = new Random();
        
        //Random size
        float scale = (float) GD.RandRange(70.0, 85.0) * 0.01f;
        this.Scale = new Vector2(scale, scale);
        
        //random rotate speed
        rotationSpeed = r.Next(1, 8);
        if (r.Next(0, 2) == 1)
        {
            rotationSpeed *= -1;
        }
        //random shoot speed
        shootSpeedMod =  r.Next(60,100);
        
        //random shoot position
        shootLocation = r.Next(0, mapEdge);
        
        //random shoot angle
        shootAngle = r.Next(10, 50) * sideSpeed * -1;
        
        //angle gets flipped if left half of screen or right half
        if (r.Next(0, 2) == 1)
        {
            shootLocation *= -1;
            shootAngle *= -1;
        }
    
        //give vel

        this.Position = new Vector2(shootLocation, this.Position.y);
        vel.y = shootSpeed * shootSpeedMod * -1;
        vel.x = shootAngle;
        
        _pickupTextScene = GD.Load<PackedScene>("res://world/pickups/pickup_text.tscn");
    }

    public override void _Process(float delta)
    {

        this.Position = this.Position + (vel * delta);
        vel.y += (grav * delta);
        this.Rotation = this.Rotation + (rotationSpeed * delta * 0.5f);
        // TODO redo this
        if (this.Position.y > CurrentDepth.Depth + 500)
        {
            this.QueueFree();
        }

    }
    
    private void _BodyEntered(Node body)
    {
        if (body is Player player && !_pickedUp)
        {
            player.Heal(this);
            AsyncRoutine.Start(this, Pickup);
            
            PickupText text = _pickupTextScene.Instance<PickupText>();
            text.Text = "+HP";
            GetParent().GetParent().AddChild(text);
            text.GlobalPosition = GlobalPosition;

            _pickedUp = true;
        }
        //else if (body != GetParent())
        //{
        //    QueueFree();
        //}
    }
    
    private async Task Pickup(AsyncRoutine routine)
    {
        SetDeferred("Monitoring", false);
        SetDeferred("Monitorable", false);
        GetNode<Sprite>("Sprite").Visible = false;
        GetNode<CPUParticles2D>("CPUParticles2D").Emitting = false;
        var sound = GetNode<AAudioStreamCollection>("Pickup"); //TODO change to health pickup
            
        sound.Play();
        await routine.Signal(sound, nameof(AAudioStreamCollection.Finished));

        await routine.Delay(1.0f);
        
        QueueFree();
    }

}
