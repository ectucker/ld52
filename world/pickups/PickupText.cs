using Godot;
using System;

public class PickupText : Node2D
{
    private Label _label = null;

    private string _text = "";
    public string Text
    {
        get => _text;
        set
        {
            _text = value;
            if (_label != null)
                _label.Text = value;
        }
    }

    private float _time = 0.0f;
    
    public override void _Ready()
    {
        base._Ready();
        _label = this.FindChild<Label>();
        _label.Text = Text;
        GlobalRotation = (float)GD.RandRange(-0.2f, 0.2f);
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        _time += delta;
        if (_time > 1.5f)
            QueueFree();
    }
}
