using Godot;
using System;

public class rockTest : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private float time = 0;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        evilRock rock = GD.Load<PackedScene>("res://enemies/evilRock.tscn").Instance<evilRock>();
        rock.Position = this.Position - new Vector2(this.Position.x, -500);
        AddChild(rock);
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
 public override void _Process(float delta)
 {
     time += delta;
     if (time > 3)
     {
         time = 0;
        
         evilRock rock = GD.Load<PackedScene>("res://enemies/evilRock.tscn").Instance<evilRock>();
         AddChild(rock);
         rock.Position = this.Position - new Vector2(this.Position.x, -500);
     }
 }
}
