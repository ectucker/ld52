using Godot;
using System;

public class flower_test : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private float time = 0;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        flower fl = GD.Load<PackedScene>("res://enemies/flower.tscn").Instance<flower>();
        fl.Position = this.Position - new Vector2(this.Position.x, -1000);
        AddChild(fl);
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        time += delta;
        if (time > 10)
        {
            time = 0;
        
            flower fl = GD.Load<PackedScene>("res://enemies/flower.tscn").Instance<flower>();
            fl.Position = this.Position - new Vector2(this.Position.x, -1000);
            AddChild(fl);
        }
    }
}
