using Godot;
using System;

public class worm : enemyBaseClass
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private int dir;

    private float height;

    private CPUParticles2D _particles;

    private AAudioStreamCollection _wormDig;
    
    private Sprite _sprite1;

    private Sprite _sprite2;

  //  private CollisionPolygon2D _collisionPolygon;
    
    private float flipSprite;
    
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();
        _particles = (CPUParticles2D) FindNode("CPUParticles2D", false, true);
        _wormDig = (AAudioStreamCollection)FindNode("wormDig", false, true);
        Random r = new Random();
        dir = r.Next(0, 2);

        _sprite1 = (Sprite)this.FindNode("Sprite", false, true);
        _sprite2 = (Sprite)this.FindNode("Sprite2", false, true);
       // _collisionPolygon = (CollisionPolygon2D)this.FindNode("CollisionPolygon2D", false, true);
        _sprite2.Hide();
        flipSprite = 0;
        //height = this.GlobalPosition.y - (float)GD.RandRange(0.00, 800.0);

        if (dir == 0)
        {
           this.GlobalPosition = new Vector2(2500, this.Position.y);
           // Sprite sp = TreeExtensions.FindChild<Sprite>(this, null);
            //sp.FlipH = true;
            _sprite1.FlipH = true;
            _sprite2.FlipH = true;
            _particles.Position = new Vector2(_particles.Position.x * -1, _particles.Position.y);
            _particles.Direction = new Vector2(-1, 0);
        }

        else if (dir == 1)
        {
           this.GlobalPosition = new Vector2(-2500, this.Position.y);
        }
    }

    public override void DamagePlayer(Player player)
    {
        player.Damage(this, 1.5f);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        DebugOverlay.AddMessage(this, "worm height", height.ToString(), Colors.Aqua);
        DebugOverlay.AddMessage(this, "current x", this.Position.x.ToString(), Colors.Aqua);
        _wormDig.PlayIfNot();
        
        
        flipSprite += delta;

        if (flipSprite > 0.5f)
        {
            flipSprite = 0;
            if (_sprite1.Visible)
            {
                _sprite1.Hide();
                _sprite2.Show();
               // _collisionPolygon.Scale = new Vector2(0, -1);
            }
            else
            {
                _sprite1.Show();
                _sprite2.Hide();
                //_collisionPolygon.Scale = new Vector2(0, -1);
            }
            
        }
        
        if (dir == 0)
        {
            this.Position = new Vector2(this.Position.x - (450 * delta), this.Position.y);
            if (this.Position.x < -2500) QueueFree();
            if (_particles.GlobalPosition.x < 580)
            {
                _wormDig.PlayIfNot();
                _particles.Emitting = true;
                _particles.Position = new Vector2(_particles.Position.x + (450 * delta), _particles.Position.y);
            }

            if (this.Position.x < -50) _particles.Emitting = false;

        }
        else
        {
            this.Position = new Vector2(this.Position.x + (450 * delta), this.Position.y);
            if (this.Position.x > 2500) QueueFree();
            if (_particles.GlobalPosition.x > -580)
            {
                _wormDig.PlayIfNot();
                _particles.Emitting = true;
                _particles.Position = new Vector2(_particles.Position.x - (450 * delta), _particles.Position.y);
            }
            if (this.Position.x > 50) _particles.Emitting = false;
        }
        
        
        //MOVE DOWN WITH SPEED MATCHING DEPTH SPEED SO THIS DOESNT JUST LIKE FLY OFF THE SCREEN AS WE GO FASTER AND FASTER
       // this.Position = new Vector2(this.Position.x, this.Position.y + (125 * delta));
    }
}