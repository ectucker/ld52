using Godot;
using System;

public class flower : enemyBaseClass
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private float time;

    private float timeUp;

    private int pos;

    private float height;

    private const int wallEdge = 600;

    private AAudioStreamCollection _flowerGrunt;

    private bool soundPlayed = false;

    private Sprite _sprite1;

    private Sprite _sprite2;

    private float flipSprite;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();

        _flowerGrunt = (AAudioStreamCollection)this.FindNode("flowerGrunt", false, true);
        _sprite1 = (Sprite)this.FindNode("Sprite", false, true);
        _sprite2 = (Sprite)this.FindNode("Sprite2", false, true);

        _sprite2.Hide();
        flipSprite = 0;
        Random r = new Random();
        timeUp = 0.06f * (float) GD.RandRange(40, 85);
        pos = r.Next(50, wallEdge);

        if (r.Next(0, 2) == 1)
        {
            pos *= -1;
        }

        this.Position = new Vector2(pos, this.Position.y);
        //height = this.Position.y - (400 * r.Next(6, 9) + 200);
    }

    public override void DamagePlayer(Player player)
    {
        player.Damage(this, 1.5f);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        
        DebugOverlay.AddMessage(this, "Flower height", this.Position.y.ToString(), Colors.Aqua);
        DebugOverlay.AddMessage(this, "Target height", (height + CurrentDepth.Depth).ToString(), Colors.Aqua);
        time += delta;
        flipSprite += delta;

        if (flipSprite > 0.5f)
        {
            flipSprite = 0;
            if (_sprite1.Visible)
            {
                _sprite1.Hide();
                _sprite2.Show();
            }
            else
            {
                _sprite1.Show();
                _sprite2.Hide();
            }
            
        }
        
        if (time > timeUp)
        {
            this.Position = new Vector2(this.Position.x, this.Position.y + (350 * delta));
            if (!soundPlayed)
            {
                soundPlayed = true;
                _flowerGrunt.Play();
                //this.Position = new Vector2(this.Position.x, this.Position.y - (175 * delta));
            }
        }
        else
        {
            this.Position = new Vector2(this.Position.x, this.Position.y - (200 * delta));
         
        }
        if (this.GlobalPosition.y > (CurrentDepth.Depth + 1000))
        {
            QueueFree();
        }
        //MOVE DOWN WITH SPEED MATCHING DEPTH SPEED SO THIS DOESNT JUST LIKE FLY OFF THE SCREEN AS WE GO FASTER AND FASTER
       // this.Position = new Vector2(this.Position.x, this.Position.y + (125 * delta));

   
    }
}