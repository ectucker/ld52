using Godot;
using System;

public class batPath : Path2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private PathFollow2D follow;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        follow = TreeExtensions.FindChild<PathFollow2D>(this, null);
        Random r = new Random();
       if (r.Next(0, 2) == 0)
       {
           this.Scale = new Vector2(this.Scale.x * -1, this.Scale.y);
       }
   
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
      follow.Offset = follow.Offset + (1000 * delta);
      if (follow.UnitOffset > 0.95)
      {
          QueueFree();
      }
      
      //MOVE DOWN WITH SPEED MATCHING DEPTH SPEED SO THIS DOESNT JUST LIKE FLY OFF THE SCREEN AS WE GO FASTER AND FASTER
     // this.Position = new Vector2(this.Position.x, this.Position.y + (125 * delta));
  }
}
