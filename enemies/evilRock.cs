using Godot;
using System;

public class evilRock : enemyBaseClass
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private int rotationSpeed;

    private const int mapEdge = 600;
    
    [LiveValue(LVType.RANGE, 0.0f, 30.0f, "Rock")]
    public static float shootSpeed = 20;

    private int shootSpeedMod;
    
    private int shootAngle;

    private int shootLocation;

    private Vector2 vel = new Vector2();

    private int sideSpeed = 6;
    
    private float grav = 700;

    private AAudioStreamCollection _rockLaunch;
    
    
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();

        _rockLaunch = (AAudioStreamCollection)this.FindNode("rockLaunch", false, true);
        _rockLaunch.Play();
        Random r = new Random();
        
        //Random size
        float scale = (float) GD.RandRange(50.0, 75.0) * 0.01f;
        this.Scale = new Vector2(scale, scale);
        
        //random rotate speed
        rotationSpeed = r.Next(1, 8);
        if (r.Next(0, 2) == 1)
        {
            rotationSpeed *= -1;
        }
        //random shoot speed
        shootSpeedMod =  r.Next(60,100);
        
        //random shoot position
        shootLocation = r.Next(0, mapEdge);
        
        //random shoot angle
        shootAngle = r.Next(10, 60) * sideSpeed * -1;
        
        //angle gets flipped if left half of screen or right half
        if (r.Next(0, 2) == 1)
        {
            shootLocation *= -1;
            shootAngle *= -1;
        }
    
        //give vel

        this.Position = new Vector2(shootLocation, this.Position.y);
        vel.y = shootSpeed * shootSpeedMod * -1;
        vel.x = shootAngle;

        DebugOverlay.AddMessage(this, "Created rock at pos", shootLocation.ToString(), Colors.Aqua, 1, 1.5f);
        
    }

    public override void DamagePlayer(Player player)
    {
        player.Damage(this, 1.0f);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        DebugOverlay.AddMessage(this, "Rock position", this.Position.ToString(), Colors.Aqua);
        this.Position = this.Position + (vel * delta);
        if (vel.y < 0)
        {
            vel.y += (grav * 0.2f * delta);
        }
        else
        {
            vel.y += (grav * 0.9f * delta);
        }

        this.Rotation = this.Rotation + (rotationSpeed * delta);

       
        
        // TODO redo this
        if (this.Position.y > CurrentDepth.Depth + 500)
        {
            this.QueueFree();
        }
    }
}
