using Godot;
using System;

public class bat : enemyBaseClass
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private Vector2 vel = new Vector2();

    private float time = 0;

    private float flyTime = 0.75f;

    private bool flyAway = false;

    private Random r = new Random();

    private AAudioStreamCollection _batSounds;

    private AAudioStreamCollection _flapSounds;
    
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();
        
        time = r.Next(0, 200) * 0.01f;
        vel = new Vector2(1, 0).Rotated(r.Next(0, 100) * 0.063f);
        flyTime = r.Next(50, 100) * 0.02f;

        _batSounds = (AAudioStreamCollection)this.FindNode("batSounds", false, true);
        _flapSounds = (AAudioStreamCollection)this.FindNode("batFlap", false, true);
        _batSounds.Play();
        _flapSounds.Play();
    }

    public override void DamagePlayer(Player player)
    {
        player.Damage(this, 0.8f);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        time += delta;
        if (time > flyTime)
        {
            time = 0;

            if (flyAway)
            {
                flyAway = false;
                vel = new Vector2(1, 0).Rotated(r.Next(0, 100) * 0.063f);
                flyTime = r.Next(50, 100) * 0.01f;
            }
            else
            {
                flyAway = true;
                vel = vel.Rotated(3.13159f);
            }
        }
        else
        {
            this.Position = this.Position + (vel * 100 * delta);
        }
    }
}