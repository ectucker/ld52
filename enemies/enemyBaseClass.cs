﻿public abstract class enemyBaseClass : TypeTrackingArea2D<Player>
{
    public override void _Ready()
    {
        base._Ready();

        Connect(nameof(TypeEntered), this, nameof(_TypeEntered));
    }

     public abstract void DamagePlayer(Player player);
    
      public virtual void _TypeEntered(Player player)
      {
          DamagePlayer(player);
      }
}
