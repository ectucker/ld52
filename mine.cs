using Godot;
using System;

public class mine : enemyBaseClass
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private float _time;

    private float _timeTillExplode;

    private float _timeTillTick;

    private float _tickRate;

    private float _flashTimer;
    
    private bool _exploded = false;
    private AAudioStreamCollection _tickSound;

    private AAudioStreamCollection _explosionSound;

    private const int _wallEdge = 582;

    private Sprite mine1;

    private Sprite mine2;

    private CPUParticles2D _particles;
    
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();

        mine1 = (Sprite) FindNode("mine1", false, true);
        mine2 = (Sprite) FindNode("mine2", false, true);
        _particles = (CPUParticles2D) FindNode("CPUParticles2D", false, true);
        
        mine2.Hide();
        
        this.Monitoring = false;
        _time = 0;
        _flashTimer = 0;
        _timeTillExplode = (float)GD.RandRange(40.0, 65.0) * .1f;
        _tickSound = TreeExtensions.FindChild<AAudioStreamCollection>(this, null);
        _explosionSound = (AAudioStreamCollection) this.FindNode("Explosion", false, true);

        if (GD.RandRange(0.00, 100.00) > 50)
        {
            this.Position = new Vector2(this.Position.x + _wallEdge, this.Position.y);
            this.Rotation = (float) -Math.PI / 2;
        }
        else
        {
            this.Position = new Vector2(this.Position.x - _wallEdge, this.Position.y);
            this.Rotation = (float)  Math.PI / 2;
        }
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
      DebugOverlay.AddMessage(this, "Timer", _time.ToString(), Colors.Aqua);
      this.Position = new Vector2(this.Position.x, this.Position.y - (125 * delta));
      _time += delta;
      _timeTillTick += delta;
      _flashTimer += delta;
      if (!_exploded)
      {
          if (_time > _timeTillExplode)
          {
              DebugOverlay.AddMessage(this, "EXPLODE", "BOOM", Colors.Orange);
              this.Monitoring = true;

              //play explosion
              _explosionSound.Play();
              _exploded = true;
              _particles.Emitting = true;
              mine1.Hide();
              mine2.Hide();
              _time = 0;
          }
          else
          {

              _tickRate = Math.Abs(_time - _timeTillExplode) / 3.0f;
              if (_timeTillTick > _tickRate)
              {
                  _timeTillTick = 0;
                  _tickSound.Play();
                  _flashTimer = 0;
                  mine1.Hide();
                  mine2.Visible = true;
              }
          }
      }
      else if(_time > 4.0)
      {
          QueueFree();
      }else if (_time > 0.2)
      {
          Monitoring = false;
      }

      if (_flashTimer > 0.1 && !_exploded)
      {
          mine1.Visible = true;
          mine2.Hide();
      }
      
  }

    public override void DamagePlayer(Player player)
    {
        player.Damage(this, 3.5f);
    }

}
