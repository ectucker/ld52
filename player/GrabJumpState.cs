﻿using Godot;

public class GrabJumpState : JumpState
{
    private Vector2 _normal;
    
    public GrabJumpState(Player owner, Vector2 normal) : base(owner)
    {
        _normal = normal;
    }

    protected override Vector2 CalculateJumpVelocity()
    {
        float inputDir = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
        Vector2 jumpVel = Vector2.Zero;
        jumpVel += _normal * GROUND_JUMP_NORMAL;
        jumpVel += Vector2.Up * GROUND_JUMP_UP;
        jumpVel += Vector2.Right * inputDir * GROUND_JUMP_INPUT;
        return jumpVel;
    }
}
