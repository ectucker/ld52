﻿public class PlayerConsts
{
    [LiveValue(LVType.RANGE, 0.0f, 1.0f, "Player")]
    public static float JUMP_BUFFER_TIME = 0.1f;
    
    [LiveValue(LVType.RANGE, 0.0f, 1.0f, "Player")]
    public static float COYOTE_TIME = 0.1f;

    [LiveValue(LVType.RANGE, 0.0f, 1.0f, "Player")]
    public static float GRAB_BUFFER_TIME = 0.1f;
}
