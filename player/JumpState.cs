﻿using Godot;

public abstract class JumpState : StateNode<Player>
{
    [LiveValue(LVType.RANGE, 0.0f, 1000.0f, "Player")]
    public static float GROUND_JUMP_NORMAL = 100.0f;

    [LiveValue(LVType.RANGE, 0.0f, 1000.0f, "Player")]
    public static float GROUND_JUMP_UP = 100.0f;

    [LiveValue(LVType.RANGE, 0.0f, 1000.0f, "Player")]
    public static float GROUND_JUMP_INPUT = 100.0f;
    
    [LiveValue(LVType.RANGE, 0.0f, 1.0f, "Player")]
    public static float JUMP_HOLD_TIME = 0.2f;
    
    [LiveValue(LVType.RANGE, 0.0f, 1.0f, "Player")]
    public static float JUMP_HOLD_ADD = 0.1f;

    private Vector2 _jumpVel;
    
    private bool _released = false;
    private float _time = 0.0f;
    
    public JumpState(Player owner) : base(owner)
    {
        
    }

    protected abstract Vector2 CalculateJumpVelocity();

    public override void Enter()
    {
        float inputDir = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
        _jumpVel = CalculateJumpVelocity();
        StateOwner.Velocity += _jumpVel;
        StateOwner.SwitchSprite("Default");
        StateOwner.Sprite.Visible = false;
        StateOwner.HideAllAirSprite();
        StateOwner.AirSprite.Visible = true;
        StateOwner.AirSprite.GetNode<Node2D>("Jump").Visible = true;
        StateOwner.AirSprite.GetNode<Node2D>("Jump/Between").Visible = true;

        StateOwner.Sounds["Jump"].Play();
    }

    public override void Exit()
    {
        StateOwner.Sprite.Visible = true;
        StateOwner.AirSprite.Visible = false;
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        _time += delta;
        if (!Input.IsActionPressed("jump") || _time > JUMP_HOLD_TIME)
            _released = true;

        if (Mathf.Abs(StateOwner.Velocity.y) < 100)
        {
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Right").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Left").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Between").Visible = true;
        }
        else if (StateOwner.Velocity.x >= 0)
        {
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Right").Visible = true;
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Left").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Between").Visible = false;
        }
        else
        {
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Right").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Left").Visible = true;
            StateOwner.AirSprite.GetNode<Node2D>("Jump/Between").Visible = false;
        }
        
        if (StateOwner.InputBuffer.GrabLastPressed < PlayerConsts.GRAB_BUFFER_TIME && StateOwner.HasGrab)
        {
            StateOwner.InputBuffer.GrabLastPressed = Mathf.Inf;
            StateOwner.MovementState.ReplaceState(new FallingState(StateOwner));
            StateOwner.MovementState.PushState(new GrabState(StateOwner));
        }
        else if (StateOwner.HasGrab && StateOwner.InputBuffer.JumpLastPressed < PlayerConsts.JUMP_BUFFER_TIME)
        {
            StateOwner.InputBuffer.JumpLastPressed = Mathf.Inf;
            StateOwner.InputBuffer.GrabLastPressed = Mathf.Inf;
            StateOwner.MovementState.ReplaceState(new FallingState(StateOwner));
            StateOwner.MovementState.PushState(new GrabState(StateOwner, true));
        }
        else if (StateOwner.VelocityY > 0)
        {
            StateOwner.MovementState.ReplaceState(new FallingState(StateOwner));
        }
        else
        {
            float inputDir = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
            StateOwner.ApplyHorizontalAcceleration(inputDir * FallingState.AIR_ACCEL, FallingState.AIR_MAX_SPEED, FallingState.AIR_FRICTION, delta);
            if (!_released)
            {
                StateOwner.Velocity += _jumpVel * JUMP_HOLD_ADD * delta;
                DebugOverlay.AddMessage(this, "Jump Hold", (_jumpVel * JUMP_HOLD_ADD * delta).ToString());
            }
        }
    }
}
