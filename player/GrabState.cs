﻿using Godot;

public class GrabState : StateNode<Player>
{
    private Vector2 _normal;
    private Vector2 _pos;
    private bool _autojump = false;

    public GrabState(Player owner, bool autojump = false) : base(owner)
    {
        _autojump = autojump;
        _normal = owner.LastGrabNormal;
        _pos = owner.LastGrabPos;
    }

    public override void Enter()
    {
        StateOwner.Velocity = Vector2.Zero;
        StateOwner.GravityMod = 0.0f;
        if (!_autojump)
            StateOwner.SwitchSprite("Grab");
        
        StateOwner.Sounds["Grab"].Play();
    }

    public override void Exit()
    {
        StateOwner.GravityMod = 1.0f;
        if (!_autojump)
            StateOwner.SwitchSprite("Default");
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if (_autojump || Input.IsActionJustPressed("jump"))
        {
            StateOwner.MovementState.PopState();
            StateOwner.MovementState.PushState(new GrabJumpState(StateOwner, _normal));
        }
        else if (!Input.IsActionPressed("grab"))
        {
            StateOwner.MovementState.PopState();
        }
    }
}
