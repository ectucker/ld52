using Godot;
using System;

public class RaycastLink : RayCast2D
{
    [Export]
    private NodePath _path;

    public Node2D Node;

    public override void _Ready()
    {
        base._Ready();

        Node = GetNode<Node2D>(_path);
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if (IsColliding())
        {
            Node.Rotation = -GetCollisionNormal().AngleTo(Vector2.Up);
        }
        else
        {
            Node.Rotation = 0.0f;
        }
    }
}
