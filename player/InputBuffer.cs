﻿using Godot;

public class InputBuffer : Node
{
    public float JumpLastPressed = Mathf.Inf;

    public float GrabLastPressed = Mathf.Inf;

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        JumpLastPressed += delta;
        if (Input.IsActionJustPressed("jump"))
            JumpLastPressed = 0.0f;
        
        GrabLastPressed += delta;
        if (Input.IsActionJustPressed("grab"))
            GrabLastPressed = 0.0f;
    }
}
