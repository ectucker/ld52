using Godot;
using System;
using System.Collections.Generic;

public class DirectionalSprite : Node2D
{
    private List<RaycastLink> _links = new List<RaycastLink>();

    private string _lastVisible = "";

    private float _lastSwap = Mathf.Inf;

    public override void _Ready()
    {
        base._Ready();

        _links = new List<RaycastLink>(GetParent().FindChildren<RaycastLink>());
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        _lastSwap += delta;

        bool found = false;
        foreach (var link in _links)
        {
            if (link.IsColliding())
            {
                found = true;
                if (_lastSwap > 0.1f && _lastVisible != link.Name)
                {
                    _lastVisible = link.Name;
                    link.Node.Visible = true;
                    _lastSwap = 0.0f;
                }
                break;
            }
        }

        if (!found)
        {
            if (_lastSwap > 0.1f && _lastVisible != _links[0].Name)
            {
                _links[0].Node.Visible = true;
                _lastVisible = _links[0].Name;
            }
        }
        
        foreach (var link in _links)
        {
            link.Node.Visible = link.Name == _lastVisible;
        }
    }
}
