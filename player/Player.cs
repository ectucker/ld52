using Godot;
using System;
using System.Threading.Tasks;
using Godot.Collections;

public class Player : KinematicBody2D
{
    public StateStack<Player> MovementState;

    public Vector2 Velocity = Vector2.Zero;
    public float VelocityX
    {
        get => Velocity.x;
        set => Velocity = new Vector2(value, Velocity.y);
    }
    public float VelocityY
    {
        get => Velocity.y;
        set => Velocity = new Vector2(Velocity.x, value);
    }
    
    public Vector2 LastAccelDir { get; private set; } = Vector2.Zero;
    public float LastAccelDirX
    {
        get => LastAccelDir.x;
        private set => LastAccelDir = new Vector2(value, LastAccelDir.y);
    }
    public float LastAccelDirY
    {
        get => LastAccelDir.y;
        private set => LastAccelDir = new Vector2(LastAccelDir.x, value);
    }

    public Vector2 LastFloorNormal = Vector2.Up;
    public float LastOnFloor = Mathf.Inf;

    private GrabFinder _grabFinder;
    
    public Vector2 LastGrabNormal = Vector2.Up;
    public Vector2 LastGrabPos = Vector2.Zero;
    public bool HasGrab = false;
    
    [LiveValue(LVType.RANGE, 0.0f, 400.0f, "Player")]
    public static float GRAVITY = 50.0f;
    public float GravityMod = 1.0f;

    [LiveValue(LVType.RANGE, 0.0f, 4000.0f, "Player")]
    public static float KNOCKBACK_SCALE = 100.0f;

    public Node2D Sprite;
    public Node2D AirSprite;
    public MultiDictionary<string, Sprite> Sprites = new MultiDictionary<string, Sprite>();

    public InputBuffer InputBuffer;

    private const int MAX_HEALTH = 10;
    private int _health = MAX_HEALTH;

    private bool _invuln = false;

    public int Score = 0;

    public Dictionary<string, AAudioStreamCollection> Sounds = new Dictionary<string, AAudioStreamCollection>();

    private CPUParticles2D _hitParticles;
    private CPUParticles2D _landParticles;
    
    public int Health
    {
        get => _health;
        set => _health = Mathf.Clamp(value, 0, MAX_HEALTH);
    }
    
    public override void _Ready()
    {
        InputBuffer = this.FindChild<InputBuffer>();

        AirSprite = GetNode<Node2D>("NormalSpriteRoot/AirSprite");
        Sprite = GetNode<Node2D>("NormalSpriteRoot/Sprite");
        foreach (var sprite in Sprite.FindChildren<Sprite>())
        {
            Sprites.Add(sprite.Name, sprite);
        }

        foreach (var sound in this.FindChildren<AAudioStreamCollection>())
        {
            Sounds[sound.Name] = sound;
        }

        _grabFinder = this.FindChild<GrabFinder>();
        
        MovementState = new StateStack<Player>(this, new GroundState(this));
        AddChild(MovementState);
        
        GlobalScores.Reset();
        PauseMenu.Enabled = true;

        _hitParticles = GetNode<CPUParticles2D>("hit");
        _landParticles = GetNode<CPUParticles2D>("land");
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        Velocity += Vector2.Down * GRAVITY * GravityMod;

        Velocity = MoveAndSlide(Velocity, Vector2.Up, true, 4, Mathf.Deg2Rad(45.0f));

        // Keep track of last ground time
        LastOnFloor += delta;
        if (IsOnFloor())
        {
            if (LastOnFloor > 0.5f)
            {
                _landParticles.Restart();
                _landParticles.Emitting = true;
                Sounds["Land"].Play();
            }
            LastFloorNormal = GetFloorNormal();
            LastOnFloor = 0.0f;
        }

        // Keep track of the last time we hit something for grabs
        HasGrab = _grabFinder.HasGrab;
        if (HasGrab)
        {
            LastGrabNormal = _grabFinder.GrabNormal;
            LastGrabPos = GlobalPosition;
        }
        
        DebugOverlay.AddMessage(this, "Health", Health.ToString(), Colors.Red, 1);
    }
    
    public void ApplyHorizontalAcceleration(float acceleration, float maxSpeed, float friction, float delta)
    {
        Vector2 right = Vector2.Right;
        if (IsOnFloor())
        {
            right = GetFloorNormal().Rotated(Mathf.Deg2Rad(90.0f));
        }

        if (Mathf.Abs(right.AngleTo(Vector2.Right)) > Mathf.Deg2Rad(45.0f))
        {
            DebugOverlay.AddMessage(this, "Steep", "Yes");
            return;
        }
        
        DebugOverlay.AddMessage(this, "Right Dir", right.ToString());
        DebugOverlay.AddMessage(this, "Right Angle", right.AngleTo(Vector2.Right).ToString());

        // Current X-velocity
        Vector2 curVelX = Velocity.Project(right);
        DebugOverlay.AddMessage(this, "Cur X", curVelX.ToString());
        
        // Speed limit
        float speedX = curVelX.Length();
        float speedLimitX = Mathf.Max(speedX, maxSpeed);
        
        Vector2 accelX = right * maxSpeed * acceleration;
        Vector2 frictionX = -curVelX * friction;
        if (speedX < maxSpeed)
        {
            // If friction is resisting acceleration, don't
            if (frictionX.Normalized().Dot((accelX * right).Normalized()) < 0.0)
            {
                frictionX = Vector2.Zero;
                DebugOverlay.AddMessage(this, "Friction", "Disabled");
            }
        }

        Vector2 newVelX = curVelX;
        newVelX += accelX * delta;
        newVelX += frictionX * delta;
        newVelX = Mathf.Clamp(newVelX.Length(), -speedLimitX, speedLimitX) * newVelX.Normalized();

        Velocity = Velocity - curVelX + newVelX;
        LastAccelDirX = Mathf.Sign(acceleration);
    }

    public void Damage(Node2D source, float knockback)
    {
        if (!_invuln)
        {
            Health -= 1;
            Sounds["Damage"].Play();
            
            Velocity += ((GlobalPosition - source.GlobalPosition).Normalized() + Vector2.Up).Normalized() * knockback * KNOCKBACK_SCALE;
            if (MovementState.IsInState(typeof(GrabState)))
            {
                MovementState.PopState();
            }

            Hud.Instance.HealthPercent = Health / (float)MAX_HEALTH;

            if (Health > 0)
            {
                AsyncRoutine.Start(this, DamageAnim);
            }
            else
            {
                MovementState.PushState(new DeathState(this));
                AsyncRoutine.Start(this, DeathAnim);
            }

            _hitParticles.Emitting = true;
        }
    }

    public void Heal(Node2D source)
    {
        if (Health > 0)
        {
            Health += 1;
            
            Hud.Instance.HealthPercent = Health / (float)MAX_HEALTH;
        }
    }

    public void GivePoints(Node2D source, int num)
    {
        Score += Mathf.CeilToInt(num * 10.0f * GlobalScores.Multiplier);
        Hud.Instance.Score = Score;
    }

    private async Task DamageAnim(AsyncRoutine routine)
    {
        _invuln = true;
        
        CameraEffects.Hitstop(0.2f);
        CameraEffects.Trauma += 0.8f;

        GetNode<Node2D>("NormalSpriteRoot").Visible = false;
        GetNode<Node2D>("DamageSprite").Visible = true;
        await routine.Delay(0.5f);
        GetNode<Node2D>("NormalSpriteRoot").Visible = true;
        GetNode<Node2D>("DamageSprite").Visible = false;

        _invuln = false;
    }
    
    private async Task DeathAnim(AsyncRoutine routine)
    {
        _invuln = true;

        GlobalScores.Finished = true;
        
        GetNode<Node2D>("NormalSpriteRoot").Visible = false;
        GetNode<Node2D>("DamageSprite").Visible = false;
        
        GetParent().GetNode<AnimationPlayer>("GameOverPlayer").Play("GameOver");
        await routine.Delay(3.3f);
        
        Transition.TransitionTo("res://UI/end_screen/end_screen.tscn");

        _invuln = false;
    }

    public void SwitchSprite(string name)
    {
        foreach (var sprite in Sprites.Values)
        {
            sprite.Visible = false;
        }

        var sprites = Sprites[name];
        foreach (var sprite in sprites)
        {
            sprite.Visible = true;
        }
    }

    public void SwitchDirection(string name)
    {
        foreach (Node2D child in Sprite.GetChildren())
        {
            child.Visible = child.Name == name;
        }
    }

    public void HideAllAirSprite()
    {
        foreach (Node2D sprite in AirSprite.GetChildren())
        {
            sprite.Visible = false;
        }
    }
}
