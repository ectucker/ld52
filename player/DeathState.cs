﻿using Godot;

public class DeathState : StateNode<Player>
{
    public DeathState(Player owner) : base(owner)
    {
    }

    public override void Enter()
    {
        StateOwner.CollisionLayer = 0;
        StateOwner.CollisionMask = 0;
        StateOwner.GetNode<Node2D>("NormalSpriteRoot").Visible = false;
        StateOwner.GetNode<Node2D>("DeathSprite").Visible = true;
    }

    public override void Exit()
    {
    }
}
