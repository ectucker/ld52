using Godot;
using System;

public class SquashManager : Node2D
{
    private Node2D _default;
    private Node2D _squash;

    private Player player;

    [Export]
    private Vector2 _normal = Vector2.Up;

    private float _squashTime = 0.0f;
    
    public override void _Ready()
    {
        _default = GetChild<Node2D>(0);
        _squash = GetChild<Node2D>(1);
        player = this.FindParent<Player>();
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if (player.Velocity.Project(_normal).Length() > 150.0f)
        {
            _squashTime = 0.3f;
        }

        _squashTime -= delta;
        if (_squashTime > 0)
        {
            _squash.Visible = true;
            _default.Visible = false;
        }
        else
        {
            _squash.Visible = false;
            _default.Visible = true;
        }
    }
}
