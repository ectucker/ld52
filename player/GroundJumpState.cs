﻿using Godot;

public class GroundJumpState : JumpState
{
    public GroundJumpState(Player owner) : base(owner)
    {
        
    }

    protected override Vector2 CalculateJumpVelocity()
    {
        float inputDir = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
        Vector2 jumpVel = Vector2.Zero;
        jumpVel += StateOwner.LastFloorNormal * GROUND_JUMP_NORMAL;
        jumpVel += Vector2.Up * GROUND_JUMP_UP;
        jumpVel += Vector2.Right * inputDir * GROUND_JUMP_INPUT;
        return jumpVel;
    }
}