﻿using Godot;

public class GroundState : StateNode<Player>
{
    [LiveValue(LVType.RANGE, 0.0f, 200.0f, "Player")]
    public static float GROUND_ACCEL = 50.0f;
    
    [LiveValue(LVType.RANGE, 0.0f, 800.0f, "Player")]
    public static float GROUND_MAX_SPEED = 50.0f;

    [LiveValue(LVType.RANGE, 0.0f, 100.0f, "Player")]
    public static float GROUND_FRICTION = 50.0f;
    
    public GroundState(Player owner) : base(owner)
    {
        
    }

    public override void Enter()
    {
        StateOwner.SwitchSprite("Default");
        StateOwner.SwitchDirection("DownSnap");
        
        //StateOwner.Sounds["Land"].Play();
    }

    public override void Exit()
    {
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
        
        if (Input.IsActionPressed("grab") && StateOwner.HasGrab)
        {
            StateOwner.MovementState.PushState(new GrabState(StateOwner));
        }
        else if (StateOwner.IsOnFloor())
        {
            float inputDir = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
            DebugOverlay.AddMessage(this, "Ground Accel", GROUND_ACCEL.ToString());
            DebugOverlay.AddMessage(this, "GroundMaxSpeed", GROUND_MAX_SPEED.ToString());
            StateOwner.ApplyHorizontalAcceleration(inputDir * GROUND_ACCEL, GROUND_MAX_SPEED, GROUND_FRICTION, delta);
            if (StateOwner.InputBuffer.JumpLastPressed < PlayerConsts.JUMP_BUFFER_TIME)
            {
                StateOwner.InputBuffer.JumpLastPressed = Mathf.Inf;
                StateOwner.MovementState.PushState(new GroundJumpState(StateOwner));
            }
        }
        else
        {
            StateOwner.MovementState.PushState(new FallingState(StateOwner));
        }
    }
}
