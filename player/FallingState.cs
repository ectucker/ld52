﻿using Godot;

public class FallingState : StateNode<Player>
{
    [LiveValue(LVType.RANGE, 0.0f, 200.0f, "Player")]
    public static float AIR_ACCEL = 50.0f;
    
    [LiveValue(LVType.RANGE, 0.0f, 800.0f, "Player")]
    public static float AIR_MAX_SPEED = 75.0f;

    [LiveValue(LVType.RANGE, 0.0f, 10.0f, "Player")]
    public static float AIR_FRICTION = 10.0f;
    
    public FallingState(Player owner) : base(owner)
    {
        
    }

    public override void Enter()
    {
        StateOwner.SwitchSprite("Default");
        StateOwner.Sprite.Visible = false;
        StateOwner.HideAllAirSprite();
        StateOwner.AirSprite.Visible = true;
        StateOwner.AirSprite.GetNode<Node2D>("Fall").Visible = true;
        StateOwner.AirSprite.GetNode<Node2D>("Fall/Between").Visible = true;
    }

    public override void Exit()
    {
        StateOwner.Sprite.Visible = true;
        StateOwner.AirSprite.Visible = false;
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        float inputDir = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
        StateOwner.ApplyHorizontalAcceleration(inputDir * AIR_ACCEL, AIR_MAX_SPEED, AIR_FRICTION, delta);

        if (Mathf.Abs(StateOwner.Velocity.y) < 200)
        {
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Right").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Left").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Between").Visible = true;
        }
        else if (StateOwner.Velocity.x >= 0)
        {
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Right").Visible = true;
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Left").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Between").Visible = false;
        }
        else
        {
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Right").Visible = false;
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Left").Visible = true;
            StateOwner.AirSprite.GetNode<Node2D>("Fall/Between").Visible = false;
        }
        
        if (StateOwner.InputBuffer.GrabLastPressed < PlayerConsts.GRAB_BUFFER_TIME && StateOwner.HasGrab)
        {
            StateOwner.InputBuffer.GrabLastPressed = Mathf.Inf;
            StateOwner.MovementState.PushState(new GrabState(StateOwner));
        }
        else if (StateOwner.HasGrab && StateOwner.InputBuffer.JumpLastPressed < PlayerConsts.JUMP_BUFFER_TIME)
        {
            StateOwner.InputBuffer.JumpLastPressed = Mathf.Inf;
            StateOwner.InputBuffer.GrabLastPressed = Mathf.Inf;
            StateOwner.MovementState.PushState(new GrabState(StateOwner, true));
        }
        else if (StateOwner.InputBuffer.JumpLastPressed < PlayerConsts.JUMP_BUFFER_TIME && StateOwner.LastOnFloor < PlayerConsts.COYOTE_TIME)
        {
            StateOwner.InputBuffer.JumpLastPressed = Mathf.Inf;
            StateOwner.MovementState.ReplaceState(new GroundJumpState(StateOwner));
        }
        else if (StateOwner.IsOnFloor())
        {
            StateOwner.MovementState.PopState();
        }
    }
}