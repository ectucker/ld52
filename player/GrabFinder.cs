using Godot;
using System;
using System.Collections.Generic;

public class GrabFinder : Node2D
{
    private List<RayCast2D> _casts = new List<RayCast2D>();

    public override void _Ready()
    {
        base._Ready();

        _casts = new List<RayCast2D>(this.FindChildren<RayCast2D>());
    }

    public bool HasGrab
    {
        get
        {
            foreach (RayCast2D cast in _casts)
            {
                if (cast.IsColliding())
                    return true;
            }

            return false;
        }
    }
    
    public Vector2 GrabNormal
    {
        get
        {
            Vector2 dir = Vector2.Zero;
            int count = 0;
            foreach (RayCast2D cast in _casts)
            {
                if (cast.IsColliding())
                {
                    dir += cast.GetCollisionNormal();
                    count += 1;
                }
            }
            
            if (count == 0)
                return Vector2.Zero;

            return dir.Normalized();
        }
    }
}
